package kadai;

public class PoisonMatango11_3 extends Matango11_3 {

   private int poisonCount = 5;
   public PoisonMatango11_3(char suffix) {
	  super(suffix);
   }
   public void attack(Hero h) {
	  super.attack(h);
	  if (this.poisonCount > 0) {
		System.out.println("さらに毒の胞子をばらまいた");
		int dmg = h.getHp() / 5;
		h.setHp(h.getHp() - dmg);
		System.out.println(dmg + "ポイントのダメージをあたえた！");
		this.poisonCount--;
	  }
   }
}
