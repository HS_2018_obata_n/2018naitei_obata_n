package kadai12_3;

public interface Thing {
  double getWeight();
  void setWeight(double weight);
}
