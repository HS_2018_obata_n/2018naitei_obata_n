package kadai;

public class Matango11_3 {

  int hp = 50;
  private char suffix;
  public Matango11_3(char suffix) {
	 this.suffix = suffix;
  }
  public void attack(Hero h) {
	 System.out.println("キノコ" + this.suffix + "の攻撃");
	 System.out.println("10のダメージ");
	 h.setHp(h.gethp() - 10);
  }
}
