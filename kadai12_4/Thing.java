package kadai12_4;

public interface Thing {
   double getWeight();
   void setWeight(double weight);
}
