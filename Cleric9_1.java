package kadai;

public class Cleric9_1 {

  String name;
  int hp = 50;
  int mp = 10;
  static final int MAX_HP = 50;
  static final int MAX_MP = 10;

  public Cleric9_1(String name,int hp,int mp) {
    this.name = name;
    this.hp = hp;
    this.mp = mp;
  }

  public Cleric9_1(String name, int hp) {
	 this(name, hp, Cleric9_1.MAX_MP);
  }
  public Cleric9_1(String name) {
	 this(name, Cleric9_1.MAX_HP);
  }
}

